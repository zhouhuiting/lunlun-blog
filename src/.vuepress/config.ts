import { defineUserConfig } from "vuepress";
import theme from "./theme";

export default defineUserConfig({
  lang: "zh-CN",
  title: "伦伦吖",
  description: "伦伦吖的博客",

  base: "/lunlun-blog/",

  theme,
});
